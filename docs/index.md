---
home: true
images:
  # Fargekode: #e65741
  - src: ./undraw_reading_time_gvg0.svg
    alt: Illustrasjonsbilde - menneske som leer
  - src: ./undraw_Master_plan_re_jvit.svg
    alt: Illustrasjonsbilde - menneske som tenker
tagline: Dokumentasjon for datatjenester fra Biblioteksentralens metadatabrønn Ája
actions:
  - text: Hente data →
    link: /hente/
  - text: Levere  data →
    link: /levere/
# features:
#   - title: Feature 1 Title
#     details: Feature 1 Description
#   - title: Feature 2 Title
#     details: Feature 2 Description
#   - title: Feature 3 Title
#     details: Feature 3 Description
footer: >
  <a href="https://www.bibsent.no/">Biblioteksentralen</a>
---
