# Levere data til Biblioteksentralen

Som forlag kan du levere metadata for nyutgivelser til Biblioteksentralen kostnadsfritt.

Data i form av Excel-filer eller lignende kan sendes til [bsforlagsdata@bibsent.no](mailto:bsforlagsdata@bibsent.no). Bruk gjerne [Excel-malen vår](/vedlegg/mal_nyutgivelser_til_biblioteksentralen_v6.xlsx) hvis du har mulighet til det.

Ta kontakt med [bsforlagsdata@bibsent.no](mailto:bsforlagsdata@bibsent.no) hvis du har et API vi kan hente data fra.
