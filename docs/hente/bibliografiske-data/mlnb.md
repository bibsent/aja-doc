---
sidebarDepth: 0
title: Åpne data
permalink: /hente/bibliografiske-data/mlnb.html
redirectFrom: 
  - /hente/mlnb/
---
# Åpne data gjennom Nasjonalbibliotekets metadataleveranse

Fra 1. november 2020 leverer Biblioteksentralen åpne bibliografiske data for Nasjonalbibliotekets metadataleveranse. Her finner du informasjon om hvordan du kan hente og bruke dataene.

## Hvordan kan dataene hentes?

Bibliografiske data i form av MARC 21-katalogposter er tilgjengelige via standardiserte programmeringsgrensesnitt (API-er).
Data kan høstes via [OAI-PMH](./oai-pmh.md) eller søkes i via [SRU](./sru.md).

Lenker til [omslagsbilder](../omslagsbilder.md) leveres i katalogpostene.

## Hvordan kan dataene brukes?

De bibliografiske postene er lisensisert under [CC-0](https://creativecommons.org/publicdomain/zero/1.0/deed.no),
som betyr at de kan brukes og gjenbrukes fritt, selv for kommersielle formål, uten å spørre om tillatelse.

Omslagsbilder kan kun brukes fritt innenfor rammene fremforhandlet av Nasjonalbiblioteket.
[Her kommer det en lenke til en side hos Nasjonalbiblioteket som beskriver disse rammene, når de får laget en slik side]
